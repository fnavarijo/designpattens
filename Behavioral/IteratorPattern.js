// using Iterator
class IteratorClass {
    constructor(data) {
        this.index = 0;
        this.data = data;
    }

    [Symbol.iterator]() {
        return {
            next: () => {
                if (this.index < this.data.length) {
                    return { value: this.data[this.index++], done: false };
                } else {
                    this.index = 0; // to reset iteration status
                    return { done: true };
                }
            },
        };
    }
}

// using Generator
function* iteratorUsingGenerator(collection) {
    var nextIndex = 0;

    while (nextIndex < collection.length) {
        yield collection[nextIndex++];
    }
}

// usage Class
const iteratorUse = new IteratorClass(['Hi', 'Hello', 'Bye'])[Symbol.iterator]();

// console.log([...iteratorUse]); // 'Hi'
console.log(iteratorUse.next().value); // 'Hi'
console.log(iteratorUse.next().value); // 'Hello'
console.log(iteratorUse.next().value); // 'Bye'
console.log(iteratorUse.next().value); // Undefined



// usage
const gen = iteratorUsingGenerator(['Hi', 'Hello', 'Bye']);

console.log(gen.next().value); // 'Hi'
console.log(gen.next().value); // 'Hello'
console.log(gen.next().value); // 'Bye'
console.log(gen.next().value); // 'Bye'