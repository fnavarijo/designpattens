// using Object.create as was recommended by ES5 standard
const car = {
    noOfWheels: 4,
    start() {
        return 'started';
    },
    stop() {
       return 'stopped';
    },
    // printme() {
    //     console.log('ME', this);
    // }
};
  
// Object.create(proto[, propertiesObject])
  
const myCar = Object.create(car, { owner: { value: 'John' }, horn: { value: function () { return 'Beep' } }, printme: { value: function () { console.log(this) } } });
  
// console.log(myCar.__proto__ === car); // true
console.log('Owner: ', myCar.owner);
console.log('Horn: ', myCar.horn());
console.log('Car Status: ', myCar.start());
console.log('Car Me: ', myCar.printme());
console.log('Car Status: ', myCar.stop());